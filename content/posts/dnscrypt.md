---
title: "DNSCrypt - running the server"
description: "How we run a DNSCrypt server using docker-compose"
date: 2021-08-06T23:38:45+02:00
author: wanderer - https://git.dotya.ml/wanderer
draft: false
toc: true
enableGitInfo: true
tags: [dnscrypt, dns, privacy, security, censorship]
---

## why are you doing this?
There are many publicly available [open resolvers using DoT, DoH or
DNSCrypt](https://dnscrypt.info/public-servers) just sitting around the
interwebs, waiting to secure the DNS traffic and protect it from whoever is
looking.

However, we have still felt the need to run our own, especially since
DNS is such a critical piece of infrastructure.

And now we're offering it for public use.

## so what is it?
What we're running is a non-censoring, non-logging, DNSSEC-capable, DNSCrypt-enabled DNS
resolver using
[dnscrypt-server-docker](https://github.com/dnscrypt/dnscrypt-server-docker) project.
Of course, our resolver is available over both IPv4 and IPv6.

## can I haz some plz
Yes! As a matter of fact, you should even be able to get records on
[OpenNIC](https://www.opennic.org/) domains.

You can try some using the awesome tool [`doggo`](https://github.com/mr-karan/doggo), like so:
```shell
doggo --debug --json NS epic. @sdns://AQcAAAAAAAAAETE0NC45MS43MC42Mjo1NDQzIHF-JiN46cNwFXJleEVWGWgrhe2QeysUtZoo9HwzYCMzITIuZG5zY3J5cHQtY2VydC5kbnNjcnlwdC5kb3R5YS5tbA
```
example response:
```shell
DEBUG[2022-09-01T00:22:23+02:00] initiating DNSCrypt resolver

DEBUG[2022-09-01T00:22:23+02:00] Starting doggo 🐶

DEBUG[2022-09-01T00:22:23+02:00] Attempting to resolve domain=epic. nameserver="144.91.70.62:5443" ndots=0
[
    {
        "answers": [
            {
                "name": "epic.",
                "type": "NS",
                "class": "IN",
                "ttl": "86400s",
                "address": "ns13.opennic.glue.",
                "status": "",
                "rtt": "45ms",
                "nameserver": "144.91.70.62:5443"
            }
        ],
        "authorities": null,
        "questions": [
            {
                "name": "epic.",
                "type": "NS",
                "class": "IN"
            }
        ]
    }
]
```

### `dnscrypt-proxy` configuration tips
If you'd, for some reason, like to use exclusively our name servers, simply set
the `server_names` in the root section of your `dnscrypt-proxy.toml` config
file:
```toml
server_names = ['dotya.ml', 'dotya.ml-ipv6']
```

By default servers are picked based on latency, which is a sane default and it
is in fact what we use.

If in need of more granular nameserver selection based on anything other than
latency they can additionally easily be filtered (without being explicitly
listed) based on:
* logging
* filtering
* DNSSEC capabilities
* DoH, ODoH or DNSCrypt capabilities
* IPv4 or IPv6 availability

Further, we also remove certain players from the equation by simply listing
them in `disabled_server_names`, like so:
```toml
disabled_server_names = ['google-ipv6', 'cloudflare', 'cloudflare-ipv6', 'cisco', 'cisco-ipv6', 'cisco-familyshield', 'cisco-familyshield-ipv6', 'yandex', 'apple', 'doh.dns.apple.com']
```

### old news
> Update 2022-09-01: the servers are now a part of the official listing at
> https://dnscrypt.info/public-servers/, so there is no point in adding them
> manually anymore. Keeping this for posterity.

Paste one or both of the following entries in the `[static]` section of your
`dnscrypt-proxy.toml` configuration file.

```toml
[static]
  # IPv4 (144.91.70.62, port 5443)
  [static. 'dnscrypt.dotya.ml-ipv4']
    stamp = 'sdns://AQcAAAAAAAAAETE0NC45MS43MC42Mjo1NDQzIHF-JiN46cNwFXJleEVWGWgrhe2QeysUtZoo9HwzYCMzITIuZG5zY3J5cHQtY2VydC5kbnNjcnlwdC5kb3R5YS5tbA'

  # IPv6 (2a02:c207:2030:396::1, port 5443)
  [static. 'dnscrypt.dotya.ml-ipv6']
    stamp = 'sdns://AQcAAAAAAAAAHFsyYTAyOmMyMDc6MjAzMDozOTY6OjFdOjU0NDMgcX4mI3jpw3AVcmV4RVYZaCuF7ZB7KxS1mij0fDNgIzMhMi5kbnNjcnlwdC1jZXJ0LmRuc2NyeXB0LmRvdHlhLm1s'
```

## server configuration
Files used to set up and run this service can be found here:\
https://git.dotya.ml/dotya.ml/dnscrypt-server.
It's a `docker-compose` setup managed with `systemd`, similar to how Drone CI
is handled.

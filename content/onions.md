---
title: "onions 🧅"
description: "summary of onion services dotya.ml provides - a more anonymous way to access your stuff"
date: 2022-08-30T12:00:42+02:00
lastmod: 2022-08-30T12:00:42+02:00
enableGitInfo: true
draft: false
---

> Note: This is a work in progress - more services are to come

> Note 2: the `http` part of the links below is misleading, as (our) [onion
> services](https://community.torproject.org/onion-services/) are in fact
> fully encrypted every step of the way using `https`, only the certs are not
> signed by a conventional CA (certificate authority), which means
> conventional browsers (including Firefox-based TorBrowser) would cry if the
> sites were served with explicit `https` prefix.  
> this decreases the security by exactly zero and unless LetsEncrypt starts
> issuing certs for `.onion` domains, we're not going to see broad usage of
> *explicit* `https` prefix on onion services, since only the likes of NY
> Times, BBC, Twitter or Facebook are going to make their CAs to sign them a
> neat little `.onion` cert.

Gitea: http://2crftbzxbcoqolvzreaaeyrod5qwycayef55gxgzgfcpqlaxrnh3kkqd.onion\
this site: http://6426tqrh4y5uobmo5y2csaip3m3avmjegd2kpa24sadekpxglbm34aqd.onion\
prometheus: http://vognfwm7c6wq2gxqcmswi2flwckuxryefd7n3axxkvlpasdjhns5buqd.onion\
grafana: http://6t3ydf7sl7iso2wbymbfjtaq6qqlrms37ffik2siulsljc3ubobklnid.onion\
statuspage: http://o4irro4dspyuytbw2b2g2ac4ukkh2ex53oolhzw7hrfjmq6tiklrtwqd.onion

#### current progress on onion drone
https://git.dotya.ml/dotya.ml/community/issues/5

Making `DroneCI` available as a hidden service would require either
a) spinning up another instance, for which we currently don't have capacities, or
b) some kind of an evil hack that we've not yet discovered.\
We're open to ideas - if you know how to make this work, please, send us a patch,
PR or an email with anything interesting and worthwile.

set-up-but-not-properly-working drone: http://c3vqfx2dqltvdbsqu3ndqwcxsp3uk3vcxo2jsigie5zfajub3j3y35id.onion

### clearnet
also check out [services]({{< relref "services" >}})...

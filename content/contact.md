---
title: "contact"
description: "ways to get in contact with dotya.ml maintainers"
date: 2020-03-07T01:53:03+01:00
draft: false
---
## \-- wanderer
```bash
echo wanderer+hello.dotya.ml | sed 's/\./@/'
```

use [age](https://github.com/FiloSottile/age) if possible.\
pubkey:
```sh
age16xdcxvnnhcekv59ncj5fmdarhm8csdgd9nk7nzxpywg5xtehq4kq49880e
```

gpg fingerprint:

```bash
E860 AB3C D007 8D30 E86C  DA74 7B28 D8DC 28BD 2388
```

the corresponding pubkey: [0x28bd2388]({{< relref "28bd2388.md" >}})  
plain key file: [0x28bd2388.asc](/store/0x28bd2388.asc)  
openpgp key server link: [0x28bd2388](https://keys.openpgp.org/vks/v1/by-fingerprint/E860AB3CD0078D30E86CDA747B28D8DC28BD2388)

## \-- 2EEEB
```bash
echo andrej.pillar,vutbr.cz | sed 's/\,/@/'
```

fingerprint:
```bash
4413 88B1 4509 04C0 E435 6F16 AA07 F3B7 1F41 8FEE
```

the pubkey can be found in a [key repo](https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xaa07f3b71f418fee)

communication using gpg is *preferable*

## Privacy
see [privacy]({{< relref "about#privacy" >}}), or in case of questions, say
hi and ask away at:
```sh
hello at dotya dot ml
```

## Long live the libre world!

Feel at home.\
Cheers.

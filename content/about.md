---
title: "about dotya.ml"
description: "an overview of dotya.ml's activities and mission"
date: 2020-08-06T17:15:03+01:00
lastmod: 2022-09-06T16:11:32+02:00
draft: false
---

Free services provided for fun as a hobby with passion and :white_heart:

### Clearnet services
see what we have so far: [list of services]({{< relref "services.md" >}}).

### Onion services
for increased privacy of our users, *some* services are also available
natively via [TOR](https://www.torproject.org/), have a look at
[onions 🧅]({{< relref "onions.md" >}}) for details.

### Observability
to enable long-term monitoring of trends for services we're running:
* [prometheus](https://prometheus.io) at https://metrics.dotya.ml
* [grafana](https://grafana.com) at https://grafana.dotya.ml

### Status
* in-house status monitor at https://status.dotya.ml (courtesy of [statping-ng](https://statping-ng.github.io))
* UptimeRobot ([affiliate link](https://uptimerobot.com/?rid=a60f8392870bc9)) hosted dashboard at https://stats.uptimerobot.com/93yPqFmmx8

### Security
[HSTS](https://tools.ietf.org/rfc/rfc6797.txt) has been enabled early on for `dotya.ml`,
which means that all major browsers (Firefox, Chromium, Safari and Opera) today know that this site,
as well as **all** of its subdomains, communicate with you only using TLS to secure the data in transit
(they check the [preload list](https://source.chromium.org/chromium/chromium/src/+/master:net/http/transport_security_state_static.json)),
i.e. no plain HTTP connections.

#### Misc
* HTTP headers insight for https://dotya.ml as per [Mozilla HTTP Observatory](https://observatory.mozilla.org/analyze/dotya.ml)\
Scoring 130 out of 100 points

* [SecurityHeaders](https://securityheaders.com/) report at https://securityheaders.com/?q=https%3A%2F%2Fdotya.ml

* SSL Labs [TLS rating](https://www.ssllabs.com/ssltest/analyze.html?d=dotya.ml): *A+*

* [cryptcheck.fr](https://cryptcheck.fr/https/dotya.ml): *A+*

### Privacy
see [privacy]({{< relref "privacy" >}}), the short version being *we are not
selling you out* 🎉.

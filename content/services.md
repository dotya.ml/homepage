---
title: "services"
description: "a non-exhaustive list of services hosted by dotya.ml available to the community"
date: 2022-08-30T11:50:50+02:00
lastmod: 2022-08-30T11:50:50+02:00
enableGitInfo: true
toc: true
draft: false
---

a non-exhaustive list of services available to the community:
* [Gitea](https://gitea.io) SCM instance at https://git.dotya.ml
* [DroneCI](https://drone.io) instance (login with a Gitea account) at https://drone.dotya.ml
* DNS resolvers:
  * [DNSCrypt](https://dnscrypt.info/) resolver (see [DNSCrypt]({{< relref "posts/dnscrypt" >}}))
  * [CoreDNS](https://coredns.io/) serving
    [DNS-over-TLS](https://www.rfc-editor.org/rfc/rfc7858) at
    `dns.dotya.ml:853`, and
    [DNS-over-HTTPS](https://en.wikipedia.org/wiki/DNS_over_HTTPS) at
    `https://dns.dotya.ml/dns-query`|`https://dns.dotya.ml:4053/dns-query`
    ([config](https://git.dotya.ml/dotya.ml/coredns)).
* [SearXNG](https://github.com/searxng/searxng) *metasearch* engine instance at https://searxng.dotya.ml/
* [tmate](https://tmate.io/) server (see https://git.dotya.ml/dotya.ml/tmate)

also check out [onions 🧅]({{< relref "onions" >}}) to learn about services accessible via TOR.

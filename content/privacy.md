---
title: "privacy"
description: "an overview of dotya.ml's information collection/use practices (spoiler - we don't sell your data)"
date: 2022-08-30T12:47:37+02:00
lastmod: 2022-08-30T12:47:37+02:00
enableGitInfo: true
draft: false
---

### tl;dr
[Access logs](https://en.wikipedia.org/wiki/Server_log) are stored for up to 30 days for the purpose of defending against abuse.

### the long version
Since I don't like such practices myself, this site *does not* collect
*any* kind of user/browser/device/user agent/network identifier,
which - for simplicity's sake is - no data at all - **FOR PROFIT**.\
Period.

No personally identifiable data is collected - actively or passively - and therefore can't be passed on to third parties (such as advertisement companies), nor is there any intention of *ever* doing so.

Visitor device's apparent IP address *is stored* in access log, along with a [user agent](https://duckduckgo.com/?t=ffab&q=user+agent) string, which allows us to defend against abuse.  
These logs are automatically overwriten approximately every 30 days.

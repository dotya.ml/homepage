# [dotya.ml](https://git.dotya.ml/dotya.ml/homepage/)

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/dotya.ml/homepage/status.svg?ref=refs/heads/master)](https://drone.dotya.ml/dotya.ml/homepage)
[![Mozilla HTTP Observatory Grade](https://img.shields.io/mozilla-observatory/grade-score/dotya.ml)](https://observatory.mozilla.org/analyze/dotya.ml)
[![Security Headers](https://img.shields.io/security-headers?url=https%3A%2F%2Fdotya.ml)](https://securityheaders.com/?q=https%3A%2F%2Fdotya.ml)
![Chromium HSTS preload](https://img.shields.io/hsts/preload/dotya.ml)

sawce that makes up the dotya.ml landing page.

Built with [Hugo](https://gohugo.io) on [Hermit](https://themes.gohugo.io/hermit/) theme.


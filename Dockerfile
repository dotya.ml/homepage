# syntax=docker/dockerfile-upstream:master-labs
# refs:
#   https://docs.docker.com/develop/develop-images/build_enhancements/#overriding-default-frontends
#   https://pythonspeed.com/articles/docker-buildkit/
FROM docker.io/alpine/git:v2.36.2 AS submodules

WORKDIR /homepage

COPY . .

RUN git submodule init && \
	git submodule update --recursive

FROM docker.io/immawanderer/alpine-hugo:hugo-v0.115.3 AS hugobuild

COPY --from=submodules /homepage/ /homepage/

WORKDIR /homepage

RUN hugo version && \
	hugo --minify --gc=true --cleanDestinationDir

FROM docker.io/library/golang:1.21.0-alpine3.18 AS gobuild
COPY --from=hugobuild /homepage/ /homepage/

WORKDIR /homepage

ARG VCS_REF=development

RUN CGO_ENABLED=0 GOFLAGS='-trimpath -mod=readonly -modcacherw' \
	go build -o homepage-app -v -ldflags "-s -w -X main.version=$VCS_REF" .

FROM scratch
COPY --from=gobuild /homepage/homepage-app /homepage

ENTRYPOINT ["/homepage"]
